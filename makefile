#LUA_CFLAGS =`pkg-config lua5.1 --cflags`
LUA_CFLAGS = -I/usr/include/lua5.1
CPPFLAGS += -O2 -fpic -c
CPPFLAGS +=$(LUA_CFLAGS)

LDFLAGS += -O -shared -fpic
PROG='myhello.so'

all:
	g++ $(CPPFLAGS) -o module.o module.c
	g++ $(LDFLAGS) -o $(PROG) module.o -llua5.1

clean:
	@rm -f *.o
	@rm -f $(PROG)
