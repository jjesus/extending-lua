// myhello.cpp  ->  myhello.dll (msvc6 : dll example project, select c++:codegen:"multithreaded dll" and add luabin include&lib paths)

#include <stdio.h>

extern "C" {
	#include "lua.h"
	#include "lauxlib.h"
}

#define PROJECT_TABLENAME "myhello"
#ifdef WIN32
#define LUA_API __declspec(dllexport)
#else
#define LUA_API
#endif

//#define PROJECT_TABLENAME "myhello"

extern "C" {
	int LUA_API luaopen_myhello (lua_State *L);
}

static int helloworld (lua_State *L) {
	printf("hello world!\n");
	return 0;
}

int LUA_API luaopen_myhello (lua_State *L) {
	struct luaL_reg driver[] = {
		{"helloworld", helloworld},
		{NULL, NULL},
	};
	luaL_openlib (L, "myhello", driver, 0);
	return 1;
}

/* test.lua :
require("myhello")
myhello.helloworld()
*/

