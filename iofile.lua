--[[

From the source, the Lua iolib passes regular
file handles to C.

http://www.lua.org/source/5.1/liolib.c.html

> myfile=io.open("jj.txt")

> print(myfile)
file (0x8f06bc0)

> myfile:close()

> print(myfile)
file (closed)


]]--




